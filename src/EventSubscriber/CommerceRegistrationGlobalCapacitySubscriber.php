<?php

declare(strict_types=1);

namespace Drupal\commerce_registration_global_cap\EventSubscriber;

use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\registration\Entity\RegistrationSettings;
use Drupal\registration\Event\RegistrationDataAlterEvent;
use Drupal\registration\Event\RegistrationEvents;
use Drupal\registration\Plugin\Field\RegistrationItemFieldItemList;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * CommerceRegistrationGlobalCapacitySubscriber class subscriber.
 */
final class CommerceRegistrationGlobalCapacitySubscriber implements EventSubscriberInterface {

  /**
   * Constructs a CommerceRegistrationGlobalCapacitySubscriber object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly RouteMatchInterface $routeMatch,
  ) {}

  /**
   * Registration space Alter request event handler.
   */
  public function registrationSpaceUsageAlter(RegistrationDataAlterEvent $event): void {

    $states = [];

    $product = $this->getProduct($event);
    if ($product instanceof Product) {
      // Get all variants.
      $variants = $product->getVariations();
      $other_variant_reserved = 0;
      $is_global = FALSE;
      foreach ($variants as $variant) {
        $fields = $variant->getFields();
        foreach ($fields as $id => $field) {
          if ($field instanceof RegistrationItemFieldItemList) {
            $host_entity = $this->entityTypeManager
              ->getHandler('registration', 'host_entity')
              ->createHostEntity($variant->get($id)->getEntity(), NULL);
            $variantSettings = $host_entity->getSettings();
            if (!$is_global) {
              $is_global = (bool) $variantSettings->getSetting('global_capacity');
            }
            if ($registration_type = $host_entity->getRegistrationType()) {
              $states = $registration_type->getActiveOrHeldStates();
            }

            // Ensure we have active states before querying against them.
            if (empty($states)) {
              $event->setData(0);
            }

            $database = Database::getConnection();
            $query = $database->select('registration')
              ->condition('entity_id', $host_entity->id())
              ->condition('entity_type_id', $host_entity->getEntityTypeId())
              ->condition('state', array_keys($states), 'IN');
            $query->addExpression('sum(count)', 'spaces');

            $spaces = $query->execute()->fetchField();
            $spaces = empty($spaces) ? 0 : $spaces;
            $other_variant_reserved = $spaces + $other_variant_reserved;
          }
        }
      }

      // Update only if global settings is enabled.
      if ($is_global) {
        $event->setData($other_variant_reserved);
      }
    }
  }

  /**
   * Alter space event handler.
   *
   * @param \Drupal\registration\Event\RegistrationDataAlterEvent $event
   *   Event class.
   *
   * @return void
   *   Return noting as data has been set in event itself.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function registrationSpaceAlter(RegistrationDataAlterEvent $event): void {
    $product = $this->getProduct($event);
    $is_global = FALSE;
    if ($product instanceof Product) {
      // Get all variants.
      $variants = $product->getVariations();
      $other_variant_reserved = 0;
      foreach ($variants as $variant) {
        $fields = $variant->getFields();
        foreach ($fields as $id => $field) {
          if ($field instanceof RegistrationItemFieldItemList) {
            $host_entity = $this->entityTypeManager
              ->getHandler('registration', 'host_entity')
              ->createHostEntity($variant->get($id)->getEntity(), NULL);
            $variantSettings = $host_entity->getSettings();
            if (!$is_global) {
              $is_global = (bool) $variantSettings->getSetting('global_capacity');
            }
            $other_variant_reserved = $other_variant_reserved + $host_entity->getActiveSpacesReserved();
          }
        }
      }
      $spaces_remaining = 0;
      if (isset($event->getContext()['settings'])) {
        $spaces_remaining = $event->getContext()['settings']->getSetting('capacity') - $other_variant_reserved;
        $spaces_remaining = $spaces_remaining < 0 ? 0 : $spaces_remaining;
      }

      // Update only if global settings is enabled.
      if ($is_global) {
        $event->setData($spaces_remaining);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RegistrationEvents::REGISTRATION_ALTER_SPACES_REMAINING => ['registrationSpaceAlter'],
      RegistrationEvents::REGISTRATION_ALTER_USAGE => ['registrationSpaceUsageAlter'],
    ];
  }

  /**
   * Get product from registration.
   *
   * @param \Drupal\registration\Event\RegistrationDataAlterEvent $event
   *   Event class.
   *
   * @return mixed|null
   *   Return data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getProduct(RegistrationDataAlterEvent $event) {
    $settings = $event->getContext()['settings'];
    if ($settings instanceof RegistrationSettings) {
      $entity_type_id = $settings->getHostEntityTypeId();
      $entity_id = $settings->getHostEntityId();

      // @todo New variants are having empty entity_id. need to check best way to get parent entity.
      $product = NULL;
      if (!$entity_id) {
        $product = $this->routeMatch->getParameter('commerce_product');
      }

      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      if ($baseVariant = $storage->load($entity_id)) {
        $product = $baseVariant->getProduct();
      }

      return $product;
    }
    return NULL;
  }

}
