<?php

namespace Drupal\commerce_registration_global_cap\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Prevents event capacity less than the number of existing registrations.
 *
 * @Constraint(
 *   id = "GlobalCapacity",
 *   label = @Translation("Enforce global registration settings capacity", context = "Validation")
 * )
 */
class GlobalCapacityConstraint extends Constraint {

  /**
   * Violation message.
   *
   * @var string
   */
  public string $message = "There are one or more variants having different capacity. Make sure all the variant have same capacity";

  /**
   * Violation message.
   *
   * @var string
   */
  public string $twoGlobal = "Global variant already defined. More than one global variant is not allowed.";

}
