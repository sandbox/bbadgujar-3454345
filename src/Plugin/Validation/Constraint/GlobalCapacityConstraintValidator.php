<?php

namespace Drupal\commerce_registration_global_cap\Plugin\Validation\Constraint;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\registration\Entity\RegistrationSettings;
use Drupal\registration\Plugin\Field\RegistrationItemFieldItemList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the GlobalCapacityConstraint constraint.
 */
class GlobalCapacityConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Current route service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Constructs the GlobalCapacityConstraintValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): GlobalCapacityConstraintValidator {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($settings, Constraint $constraint) {
    if (($settings instanceof RegistrationSettings) && ((int) $settings->getSetting('capacity') > 0)) {
      $entity_type_id = $settings->getHostEntityTypeId();

      $entity_id = $settings->getHostEntityId();

      // @todo New variants are having empty entity_id. need to check best way to get parent entity.
      if (!$entity_id) {
        $product = $this->routeMatch->getParameter('commerce_product');

        $this->productGlobalCapacity($product, $settings, $constraint);
      }

      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      if ($baseVariant = $storage->load($entity_id)) {
        $product = $baseVariant->getProduct();
        $this->productGlobalCapacity($product, $settings, $constraint);
      }
    }
  }

  /**
   * Add validation on product capacity.
   */
  protected function productGlobalCapacity($product, RegistrationSettings $settings, $constraint) {
    if ($product instanceof Product) {
      // Get all variants.
      $variants = $product->getVariations();
      if (!empty($variants)) {
        $global_capacity_check = FALSE;
        $global_capacity = 0;
        $settingsArray = [];
        if ($settings->getSetting('global_capacity')) {
          $global_capacity_check = TRUE;
          $global_capacity = $settings->getSetting('capacity');
        }
        foreach ($variants as $variant) {
          if ($variant instanceof ProductVariationInterface) {
            $fields = $variant->getFields();
            $field_key = '';
            foreach ($fields as $id => $field) {
              if ($field instanceof RegistrationItemFieldItemList) {
                $field_key = $id;
                break;
              }
            }
            if ($field_key) {
              $host_entity = $this->entityTypeManager
                ->getHandler('registration', 'host_entity')
                ->createHostEntity($variant->get($field_key)->getEntity(), NULL);
              $variantSettings = $host_entity->getSettings();
              if ($variantSettings->getSetting('global_capacity') && $global_capacity_check
                && $settings->id() != $variantSettings->id()) {
                $this->context->buildViolation($constraint->twoGlobal)->atPath('capacity')
                  ->addViolation();
                // Exit as one violation already found.
                break;
              }

              if ($variantSettings->getSetting('global_capacity') && !$global_capacity_check) {
                $global_capacity_check = TRUE;
                $global_capacity = $variantSettings->getSetting('capacity');
              }
              $settingsArray[] = [
                'capacity' => $variantSettings->getSetting('capacity'),
                'variant' => $variant->id(),
              ];
            }
          }
        }
        if ($global_capacity_check && $global_capacity > 0) {
          foreach ($settingsArray as $setting) {
            if ($setting['capacity'] != $global_capacity) {
              $this->context->buildViolation($constraint->message)->atPath('capacity')
                ->addViolation();
            }
          }
        }
      }
    }
  }

}
